import axios from "axios";

const KEY = "AIzaSyDxFjSrIIkzegi-X9vTddM6Q8FU94B457s"
export default axios.create({
    baseURL: "https://www.googleapis.com/youtube/v3",
    params: {
        part: "snippet",
        maxResults: 5,
        key: KEY
    }
})
// https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=5&q=car?key:AIzaSyDxFjSrIIkzegi-X9vTddM6Q8FU94B457s