import React, { Component } from 'react';
import Navbar from "./navbar/Navbar";
import VideoList from './video/VideoList';
import VideoDetails from "./video/VideoDetails"
export default class App extends Component {
  state = { videoList: [], selectedVideo: null }
  onSearchResult = (searchResult) => {
    console.log("in App: ", searchResult);
    this.setState({ videoList: searchResult, selectedVideo: searchResult[0] });
  }
  onVideoSelect = (video) => {
    console.log("in App onVideoSelect:", video);
    this.setState({ selectedVideo: video });
  }
  render() {
    return (
      <div>
        <Navbar onSearchResult={this.onSearchResult} />
        <div >
          <div className="row">
            <div className="col-8">
              <VideoDetails video={this.state.selectedVideo} />
            </div>
            <div className="col-4">
              <VideoList onVideoSelect={this.onVideoSelect} videoList={this.state.videoList} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
