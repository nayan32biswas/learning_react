import React from 'react'
import "./style.css"
export default function VideoItem(props) {
    var video = props.video;
    console.log(video.snippet.title);
    if (video.snippet.title.length > 35) {
        video.snippet.title = video.snippet.title.substring(0, 30) + "...";
        console.log("condition: ", video.snippet.title);
    }
    return (
        <div>
            <div onClick={() => props.onVideoSelect(props.video)} className="card flex-row flex-wrap VideoItem-item">
                <div className="card-header border-0">
                    <img src={video.snippet.thumbnails.high.url} style={{ maxHeight: "150px" }} alt={video.snippet.title}></img>
                </div>
                <div className="card-block  ">
                    <h5 className="card-title"  >{video.snippet.title}</h5>
                    <p>Publish date: {video.snippet.publishedAt}</p>
                </div>
            </div>
            <br />
        </div>
    )
}
