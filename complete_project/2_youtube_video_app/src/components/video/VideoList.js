import React from 'react'
import VideoItem from "./VideoItem"
import "./style.css"
export default function VideoList(props) {
    const renderList = props.videoList.map(video =>
        <VideoItem onVideoSelect={props.onVideoSelect} key={video.id.videoId} video={video} />
    )
    return <div>{renderList}</div>;
}
// export default VideoList;