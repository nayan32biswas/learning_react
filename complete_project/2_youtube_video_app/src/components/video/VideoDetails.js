import React from 'react'

export default function VideoDetails(props) {
    if (!props.video) {
        return <div>Loading...</div>
    }
    const { video } = props
    const videoUrls = `https://youtube.com/embed/${video.id.videoId}`;
    return (
        <div>
            {/* style={{ maxWidth: "30rem" }} */}
            <div  >
                <div className="embed-responsive embed-responsive-16by9">
                    <iframe title={video.snippet.title} className="embed-responsive-item" src={videoUrls} allowFullScreen></iframe>
                </div>
                <div className="card-body">
                    <h5 className="card-title">{video.snippet.title}</h5>
                    <p className="card-text">{video.snippet.description}</p>
                </div>
            </div>
        </div>
    )
}
