import React, { Component } from 'react';
import baseApi from '../../api/baseApi';
// import NavbarSearch from './NavbarSearch';
import SearchForm from './SearchForm'

export class Navbar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchData: []
        }
    }
    componentDidMount(){
        this.onQuerySubmit({q: "building"})
    }
    onQuerySubmit = async (searchData) => {
        this.setState({ searchData: searchData });
        const KEY = "AIzaSyDxFjSrIIkzegi-X9vTddM6Q8FU94B457s"

        var params = {
            ...searchData,
            part: "snippet",
            maxResults: 5,
            key: KEY
        };
        // const response = await baseApi.get("/search", {
        //     params: {
        //         ...searchData
        //     }
        // });
        console.log(params);
        baseApi.get("/search", {
            params: {
                ...params
            }
        }).then(response => {
            this.props.onSearchResult(response.data.items);
        }).catch(error => {
            console.log("api error: ", error);
        })
    }
    render() {
        return (
            <nav className="navbar navbar-expand-lg navbar-light bg-light h-100">
                <a className="navbar-brand" href="/">
                    Project
                    </a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <SearchForm onFormSubmit={this.onQuerySubmit} />
                </div>
            </nav>
        )
    }
}

export default Navbar

