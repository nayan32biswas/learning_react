import baseApi from "../api/baseApi";
import history from "../history";
import {
    SIGN_ID,
    SIGN_OUT,
    CREATE_STREAM,
    FETCH_STREAMS,
    FETCH_STREAM,
    DELETE_STREAM,
    EDIT_STREAM
} from "./types";

export const SignIn = (userId) => {
    return {
        type: SIGN_ID,
        payload: userId
    }
};
export const SignOut = () => {
    return {
        type: SIGN_OUT
    }
};
export const createStream = formValues => async (dispatch, getState) => {
    const { userId } = getState().auth;
    const response = await baseApi.post("/streams", { ...formValues, userId });
    dispatch({ type: CREATE_STREAM, payload: response.data });
    // redirect to home page after error less api calling
    history.push("/");
};
export const fetchStreams = () => async dispatch => {
    const response = await baseApi.get("/streams");
    dispatch({ type: FETCH_STREAMS, payload: response.data });
};
export const fetchStream = (id) => async dispatch => {
    const response = await baseApi.get(`/streams/${id}`);
    dispatch({ type: FETCH_STREAM, payload: response.data });
};
export const editStream = (id, formValues) => async dispatch => {
    const response = await baseApi.patch(`/streams/${id}`, formValues);
    dispatch({ type: EDIT_STREAM, payload: response.data });
    history.push("/");
};
export const deleteStream = (id) => async dispatch => {
    await baseApi.delete(`/streams/${id}`);
    dispatch({ type: DELETE_STREAM, payload: id });
    history.push("/");
};