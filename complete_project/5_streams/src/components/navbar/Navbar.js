import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import SearchForm from './SearchForm';
import GoogleAuth from '../GoogleAuth'

export class Navbar extends Component {

    render() {
        return (
            <nav className="navbar navbar-expand-lg navbar-light bg-light h-100">
                <Link className="navbar-brand" to="/">Streamer</Link>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <SearchForm onFormSubmit={this.getFormData} />
                    <ul className="navbar-nav ml-auto ">
                        <li className="nav-item">
                            <Link className="nav-link" to="/">All Stream</Link>
                        </li>
                        
                        <li className="nav-item">
                            <Link className="nav-link" to="/"><GoogleAuth /></Link>
                        </li>
                    </ul>

                </div>
            </nav>
        )
    }
}

export default Navbar

