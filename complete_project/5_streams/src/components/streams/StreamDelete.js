import React, { Component } from 'react';
import { Link } from "react-router-dom";
import { connect } from 'react-redux';
import history from "../../history";
import { fetchStream, deleteStream } from "../../actions";
import Modal from "../Modal";


export class StreamDelete extends Component {
    componentDidMount() {
        this.props.fetchStream(this.props.match.params.id);
    }
    renderActions() {
        const id = this.props.match.params.id;
        return (
            <>
                <button onClick={() => this.props.deleteStream(id)} type="button" className="btn btn-secondary" data-dismiss="modal">Delete</button>
                <Link to="/" type="button" className="btn btn-primary">Close</Link>
            </>
        )
    }
    render() {
        if (!this.props.stream) {
            return (
                <div>
                    <h1>Loading...</h1>
                </div>
            )
        }
        return (
            <div>
                <Modal
                    title="Stream Delete"
                    content="Are you sure want to delete this Stream"
                    actions={this.renderActions()}
                    onDismiss={() => history.push("/")}
                />
            </div>
        )
    }
}

const mapStateToProps = (state, props) => {
    console.log(props.match.params.id);
    return { stream: state.streams[props.match.params.id] }
}

const mapDispatchToProps = {
    fetchStream: fetchStream,
    deleteStream: deleteStream
}

export default connect(mapStateToProps, mapDispatchToProps)(StreamDelete)
