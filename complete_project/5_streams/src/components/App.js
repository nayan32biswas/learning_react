import React, { Component } from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import history from "../history";
import Navbar from "./navbar/Navbar";

import StreamCreate from '../components/streams/StreamCreate';
import StreamDelete from '../components/streams/StreamDelete';

import StreamEdit from '../components/streams/StreamEdit';
import StreamList from '../components/streams/StreamList';
import StreamShow from '../components/streams/StreamShow';


export default class App extends Component {
  state = { imageList: [] }
  getResult = (searchResult) => {
    this.setState({ imageList: searchResult });
  }
  render() {
    return (
      <div>
        <Router history={history}>
          <div>
            <Navbar onSearchResult={this.getResult} />
            <div className="container">
              <Switch>
                <Route path="/" exact component={StreamList} />
                <Route path="/stream/new" exact component={StreamCreate} />
                <Route path="/stream/delete/:id" exact component={StreamDelete} />
                <Route path="/stream/edit/:id" exact component={StreamEdit} />
                <Route path="/stream/:id" exact component={StreamShow} />
              </Switch>
            </div>
          </div>
        </Router>
      </div>
    );
  }
}
