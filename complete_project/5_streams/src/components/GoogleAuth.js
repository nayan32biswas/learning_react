import React, { Component } from 'react';
import { connect } from 'react-redux';
import { SignIn, SignOut } from "../actions";

class GoogleAuth extends Component {
    state = { isSignedIn: null };
    componentDidMount() {
        window.gapi.load('client:auth2', () => {
            window.gapi.client.init({
                clientId: "306589579167-0snnd3h5l2hbermgkdi39gfh8878juv0.apps.googleusercontent.com",
                scope: "email"
            }).then(() => {
                this.auth = window.gapi.auth2.getAuthInstance();
                this.onAuthChange(this.auth.isSignedIn.get());
                this.auth.isSignedIn.listen(this.onAuthChange);
            })
        })
    }
    onAuthChange = (isSignedIn) => {
        if (isSignedIn) {
            this.props.SignIn(this.auth.currentUser.get().getId());
        }
        else {
            this.props.SignOut();
        }

    }
    onSignIn = () => {
        this.auth.signIn();
    }

    onSignOut = () => {
        this.auth.signOut();
    }
    renderAuthButton() {
        if (this.props.isSignedIn === null) {
            return null;
        }
        else if (this.props.isSignedIn) {
            return (
                <button onClick={this.onSignOut} className="btn btn-danger">Sign Out</button>
            )
        }
        else {
            return (
                <button onClick={this.onSignIn} className="btn btn-primary">Sign In</button>
            )
        }
    }
    render() {
        return (
            <div>
                {this.renderAuthButton()}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return { isSignedIn: state.auth.isSignedIn };
}

export default connect(mapStateToProps, { SignIn, SignOut })(GoogleAuth)