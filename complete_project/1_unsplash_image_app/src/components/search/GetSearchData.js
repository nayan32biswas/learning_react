import moduleName from '../../api/baseApi'
export default GetSearchData = (url, props) => {
    baseApi.get(url, {
        params: {
            ...props
        }
    }).then(response => {
        console.log(response);
        return response;
    }).catch(error => {

        console.log("api error: ", error);
    })
}