import React, { Component } from 'react';

export default class SearchForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchData: ""
        }
    }
    onSubmitHandler = (event) => {
        event.preventDefault();
        this.props.onFormSubmit(this.state.searchData);
    }
    onChangeHandler = (event) => {
        var data = event.target.value;
        this.setState({ searchData: data });
    }
    render() {
        return (
            <div>
                <form className="searchForm-form form-inline" onSubmit={this.onSubmitHandler}>
                    <div className="form-group">
                        <input onChange={this.onChangeHandler} type="text" className="form-control mr-sm-2" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Start searching...." />
                        <button onClick={this.onSubmitHandler} className="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                    </div>
                </form>
            </div>
        );
    }
}
