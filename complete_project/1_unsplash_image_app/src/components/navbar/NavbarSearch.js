import React, { Component } from 'react';
import baseApi from '../../api/baseApi'
export default class NavbarSearch extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchResult: [],
            searchQyery: ""
        }
    }
    passDataToParent = (data) => {
        this.props.onSearchResult(data);
    }
    onSubmitHandler = (event) => {
        var searchQyery = event.target.value;
        this.setState({ searchQyery: searchQyery });
        console.log("searchQyery: ", event.target.value, this.state.searchQyery);
        var page = 1, per_page = 20;
        console.log("in Navbar Search funcion ");

        baseApi.get("/search/photos", {
            params: {
                query: searchQyery,
                page: page,
                per_page: per_page
            }
        }).then(response => {
            // this.setState({ searchResult: response.data.results });
            console.log("Navsearch: ",response)
            this.passDataToParent(response.data.results);


        }).catch(error => {

            console.log("api error: ", error);
        });
    }
    onChangeHandler = (event) => {
        var data = event.target.value;
        this.setState({ searchQyery: data });
    }
    render() {
        return (
            <div>
                <form className="searchForm-form form-inline" onSubmit={this.onSubmitHandler}>
                    <div className="form-group">
                        <input onChange={this.onChangeHandler} type="text" className="form-control mr-sm-2" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Start searching...." />
                        <button onClick={this.onSubmitHandler} className="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                    </div>
                </form>
            </div>
        )
    }
}
