import React, { Component } from 'react'
import "./style.css"
export default class ImageCard extends Component {
    constructor(props) {
        super(props);
        this.state = { spans: 0 };
        this.imageRef = React.createRef();
    }
    componentDidMount() {
        this.imageRef.current.addEventListener('load', this.setSpans);
    }
    setSpans = () => {
        const height = this.imageRef.current.clientHeight;
        const spans = Math.ceil(height / 15);
        this.setState({ spans });
    }
    render() {
        const { urls, alt_description } = this.props.image;
        return (
            <div style={{ gridRowEnd: `span ${this.state.spans}` }}>
                {/* <h1>{this.props.image.urls.regular}</h1> */}
                <img className="image-image" ref={this.imageRef} src={urls.regular} alt={alt_description}></img>
            </div>
        )
    }
}
