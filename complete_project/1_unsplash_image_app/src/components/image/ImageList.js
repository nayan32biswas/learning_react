import React, { Component } from 'react'
import ImageCard from "./ImageCard"
import "./style.css"
export default class ImageList extends Component {
    render() {
        return (
            <div className="image-image-list container">
                {this.props.imageList.map(image => <ImageCard key={image.id} image={image} />)}
            </div>
        )
    }
}
