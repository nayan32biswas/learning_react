import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom'
import Navbar from "./navbar/Navbar";
import ImageList from './image/ImageList';
export default class App extends Component {
  state = { imageList: [] }
  getResult = (searchResult) => {
    console.log("in App: ", searchResult);
    this.setState({ imageList: searchResult });
  }
  render() {
    return (
      <div>
        <BrowserRouter>
          <Navbar onSearchResult={this.getResult}/>
          {
            this.state.imageList.length ?
              <ImageList imageList={this.state.imageList} /> :
              <h1>There was no image</h1>
          }

        </BrowserRouter>
      </div>
    );
  }
}
