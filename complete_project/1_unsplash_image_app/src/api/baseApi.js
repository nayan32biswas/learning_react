import axios from "axios";

var token = "1dc4cd72f7fca49e3d40555f86af36f8c8ee90e8add5017b7478e3df67da807f"
export default axios.create({
    baseURL: "https://api.unsplash.com",
    headers: {
        Authorization: "Client-ID " + token
    }
})