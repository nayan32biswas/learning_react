export const selectSong = (song) => {
    console.log("selectSong: ", song);
    return {
        type: "SONG_SELECTED",
        payload: song
    }
}