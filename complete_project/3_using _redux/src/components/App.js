import React, { Component } from 'react';
import Navbar from "./navbar/Navbar";
import SongList from './song/SongList';
export default class App extends Component {
  render() {
    return (
      <div>
        <Navbar onSearchResult={this.getResult} />
        <div className="container">
          <div >
            <SongList />
          </div>
        </div>
      </div>
    );
  }
}
