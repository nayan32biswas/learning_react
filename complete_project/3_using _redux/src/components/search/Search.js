import React, { Component } from 'react';
import SearchForm from './SearchForm';
import baseApi from '../../api/baseApi'
export default class Search extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchData: []
        }
    }
    passDataToParent = (data) => {
        this.props.onSearchResult(data);
    }
    getFormData = (searchData) => {
        this.setState({ searchData: searchData });
        var page = 1, per_page=20;
        baseApi.get("/search/photos", {
            params: {
                query: searchData,
                page: page,
                per_page: per_page
            }
        }).then(response => {
            this.setState({ imageList: response.data.results });
            this.passDataToParent(response.data.results);
        }).catch(error => {

            console.log("api error: ", error);
        })
    }
    render() {
        return (
            <div>
                <SearchForm onFormSubmit={this.getFormData} />
            </div>
        )
    }
}
