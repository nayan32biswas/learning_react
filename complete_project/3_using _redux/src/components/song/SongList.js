import React, { Component } from 'react';
import { connect } from 'react-redux';
import { selectSong } from '../../actions'

import SongDetails from "./SongDetails";

class SongList extends Component {
    renderList() {
        var coun = this.props.songs.length;
        return this.props.songs.map((song) => {
            coun--;
            return (
                <div key={song.title}>
                    <div>
                        <button onClick={() => this.props.selectSong(song)} className="btn btn-primary " style={{ float: "right" }} >Select</button>
                    </div>
                    <h5 >{song.title}</h5>
                    {coun ? <hr /> : <div></div>}
                </div>
            );
        });
    }
    render() {
        return (
            <div className="row">
                <div className="col-lg-6 border py-lg-2">
                    {this.renderList()}
                </div>
                <div className="col-lg-1"></div>
                <div className="col-lg-5 border py-lg-2">
                    <SongDetails />
                </div>

            </div>
        )
    }
}
const matStateToProps = (state) => {
    return { songs: state.songs };
}
export default connect(
    matStateToProps,
    { selectSong: selectSong }
)(SongList);
