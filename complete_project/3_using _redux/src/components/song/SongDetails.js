import React from 'react';
import { connect } from 'react-redux';

const SongDetails = ({ song }) => {
    console.log(song);
    if (!song) {
        return (
            <div className="m-auto">
                <h5 >Please select a song.</h5>
            </div>
        )
    }
    return (
        <div >
            <h5>{song.title}</h5>
            <p>{song.duration}</p>
        </div>
    )

}

const mapStateToProps = (state) => {
    return { song: state.selectedSong };
}


export default connect(mapStateToProps)(SongDetails)
