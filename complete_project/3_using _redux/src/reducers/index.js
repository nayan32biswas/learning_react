import { combineReducers } from 'redux'

const songsReducer = () => {
    console.log("songsReducer");
    return [
        { title: "Hello", duration: "6:06" },
        { title: "Akcent", duration: "2.53" },
        { title: "Alan Walker", duration: "3.32" },
        { title: "Avicii", duration: "4.32" },
        { title: "Bruno Mars", duration: "4.01" },
    ]
}
const selectedSongReducer = (selectSong = null, action) => {
    console.log("selectedSongReducer: ", action);
    if (action.type === "SONG_SELECTED") {
        return action.payload;
    }
    return selectSong;
}

export default combineReducers({
    songs: songsReducer,
    selectedSong: selectedSongReducer
})