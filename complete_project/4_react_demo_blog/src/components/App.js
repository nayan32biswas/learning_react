import React, { Component } from 'react';
import Navbar from "./navbar/Navbar";
import PostList from './post/PostList'
export default class App extends Component {
  state = { imageList: [] }
  getResult = (searchResult) => {
    this.setState({ imageList: searchResult });
  }
  render() {
    return (
      <div>
        <Navbar onSearchResult={this.getResult} />
        <div className="container">
          <div>
            <PostList />
          </div>
        </div>
      </div>
    );
  }
}
