import React, { Component } from 'react';
import { connect } from 'react-redux';

export class UserHeader extends Component {
    // componentDidMount() {
    //     this.props.fetchUser(this.props.userId);
    // }
    render() {
        const user = this.props.user;
        console.log("in render: ");
        if (!user) return null;

        return (
            <div>
                <a style={{ float: "left" }} href="/"><span><i className="fas fa-user"></i></span></a>
                <h4 className="ml-2">{user.name}</h4>
            </div>
        )
    }
}

const mapStateToProps = (state, props) => {
    return { user: state.users.find(user => user.id === props.userId) }
}

// const mapDispatchToProps = {fetchUser}
// export default connect(mapStateToProps, mapDispatchToProps)(UserHeader);


export default connect(mapStateToProps)(UserHeader);
