import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchPostsAndUsers } from "../../actions";
import UserHeader from './UserHeader';

export class PostList extends Component {
    componentDidMount() {
        console.log("PostList: ", this.props.fetchPostsAndUsers());
        console.log("componentDidMount: ", this.props);
    }
    renderList() {
        return this.props.posts.map(post => {
            return (
                <div key={post.id} className="card border-dark mb-3" style={{ maxWidth: "50rem" }}>
                    <div className="card-header">
                        <UserHeader userId={post.userId} />
                    </div>
                    <div className="card-body text-dark">
                        <h5 className="card-title">{post.title}</h5>
                        <p className="card-text">{post.body}</p>
                    </div>
                </div>
            );
        });
    }
    render() {
        console.log("PostList render: ", this.props.posts);
        return (
            <div>
                {this.renderList()}
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    posts: state.posts
})

const mapDispatchToProps = {
    fetchPostsAndUsers: fetchPostsAndUsers
}

export default connect(mapStateToProps, mapDispatchToProps)(PostList)
